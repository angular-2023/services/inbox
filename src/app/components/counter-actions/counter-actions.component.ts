import { Component, OnInit } from '@angular/core';
import { InboxService } from 'src/app/services/inbox.service';

@Component({
  selector: 'app-counter-actions',
  templateUrl: './counter-actions.component.html',
  styleUrls: ['./counter-actions.component.scss'],
})
export class CounterActionsComponent implements OnInit {
  constructor(public inboxService: InboxService) {}

  ngOnInit(): void {}
}
