import { Component, OnInit } from '@angular/core';
import { InboxService } from 'src/app/services/inbox.service';

@Component({
  selector: 'app-email-counter',
  templateUrl: './email-counter.component.html',
  styleUrls: ['./email-counter.component.scss'],
})
export class EmailCounterComponent implements OnInit {
  constructor(public inboxService: InboxService) {}

  ngOnInit(): void {}

  get count() {
    return this.inboxService.getCount();
  }
}
