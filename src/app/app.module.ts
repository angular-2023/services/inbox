import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { EmailCounterComponent } from './components/email-counter/email-counter.component';
import { InboxService } from './services/inbox.service';
import { CounterActionsComponent } from './components/counter-actions/counter-actions.component';

@NgModule({
  declarations: [AppComponent, EmailCounterComponent, CounterActionsComponent],
  imports: [BrowserModule],
  providers: [InboxService],
  bootstrap: [AppComponent],
})
export class AppModule {}
