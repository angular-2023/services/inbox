import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InboxService {
  emails = 0;

  getCount() {
    return this.emails;
  }

  increment() {
    this.emails++;
  }

  decrement() {
    this.emails--;
  }
}
